import UserService  from '../../../src/services/UserService';
import UserRepository  from '../../../src/repositories/UserRepository';

jest.mock('../../../src/repositories/UserRepository');

describe('UserService tests', () => {
    const mockUserRepository = UserRepository as jest.MockedFunction<typeof UserRepository>;

    test('if getUserById userId not a number then throws exception', () => {
        expect(() => UserService.getUserById('test')).toThrow(Error);
    });

    test('if getUserById userId is a number then calls userRepository getAllUsers', () => {
        const userRepositorySpy = jest.spyOn(UserRepository, 'getUserById');
        UserRepository.getUserById(1);
        expect(userRepositorySpy).toHaveBeenCalled();
    });
});