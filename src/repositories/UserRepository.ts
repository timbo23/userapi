import { Service } from 'typedi';
import User from '../domian/User';

@Service()
class UserRepository {
    private readonly users: User[] = [
        { id: 1, name: 'user 1' },
        { id: 2, name: 'user 2' }
    ];

    private user: User[] = [];

    async getAllUsers(): Promise<User[]> {
        return this.users;
    }

    async getUserById(userId: number): Promise<User[]> {
        let user  = this.users.find(u => u.id === userId) ?? <User>{};
        this.user[0] = user;
        
        return this.user;
    }

}

export default UserRepository;