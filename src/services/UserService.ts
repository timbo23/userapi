import { Service } from 'typedi';
import User from '../domian/User';
import userRepository from '../repositories/UserRepository';
import { IUserService } from "../domian/IUserService";

@Service()
class UserService implements IUserService {
    constructor(private readonly userRepository: userRepository) {}
    
    async getAllUsers(): Promise<User[]> {
        const result = await this.userRepository.getAllUsers();

        return result;
    }

    async getUserById(userId:number): Promise<User[]>  {
        
        if(typeof userId === "number") {
            const result = await this.userRepository.getUserById(userId);
            return result;
        }

        throw new Error(`User id must be a number, recieved ${userId} instead`);
    }
}

export default UserService;