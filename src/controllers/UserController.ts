import { Request, Response } from 'express';
import { Service } from 'typedi';
import UserService  from '../services/UserService';

@Service()
class UserController {
    constructor(private readonly userService: UserService) {}

    async getAllUsers(_req: Request, res: Response) {
        const result = await this.userService.getAllUsers();

        return res.json(result);
    }

    async getUserById(_req: Request, res: Response, userId: number) {

        try {
            const result = await this.userService.getUserById(userId);
            return res.json(result);

        }catch(error) {
            res.status(500).json({ error: error })
        }

        return;
    }
}

export default UserController;