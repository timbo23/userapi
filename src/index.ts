import 'reflect-metadata';

import express from 'express';
import container from 'typedi';
import UserController from './controllers/UserController';

const appPort = 3000;

const main = async () => {
    const app = express();

    const userController = container.get(UserController);

    app.get('/users', (req, res) => userController.getAllUsers(req, res));
    app.get('/user/:userId', (req, res) => userController.getUserById(req, res, parseInt(req.params.userId) ));

    app.listen(appPort, () => {
        console.log(`Server started on port ${appPort}`);
    });

}

main().catch(error => {
    console.log(error)
});