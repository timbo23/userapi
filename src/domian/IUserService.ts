import User from "./User";

export interface IUserService {
    getUserById(userId:number): Promise<User[]>;
    getAllUsers(): Promise<User[]>;
}